sentinelsat (1.2.1-2) unstable; urgency=medium

  * Releasing debian version 1.2.1-2. (Closes: #1079824)
  * Updating years in copyright for 2024.
  * Updating to standards version 4.7.0.
  * Modernizing debhelper invocation.
  * Removing extraneous build dependency.

 -- Simon Spöhel <simon@spoehel.ch>  Mon, 09 Dec 2024 15:14:14 +0100

sentinelsat (1.2.1-1) unstable; urgency=medium

  * Releasing debian version 1.2.1-1. (Closes: #1057908)
  * Updating copyright year.
  * Adding upstream version 1.2.1.

 -- Simon Spöhel <simon@spoehel.ch>  Fri, 15 Dec 2023 18:58:55 +0100

sentinelsat (1.1.1-1) unstable; urgency=medium

  * Releasing debian version 1.1.1-1.
  * Updating copyright for 2023.
  * Updating to standards version 4.6.2.
  * Adding upstream version 1.1.1.

 -- Simon Spöhel <simon@spoehel.ch>  Sun, 08 Jan 2023 13:56:44 +0100

sentinelsat (0.14-1) unstable; urgency=medium

  * Uploading to unstable.
  * Adding upstream version 0.14.
  * Updating to debhelper 13.
  * Fixing watch file.

 -- Simon Spöhel <simon@spoehel.ch>  Thu, 06 Aug 2020 10:15:14 +0200

sentinelsat (0.13-1) unstable; urgency=medium

  * Adding debian watch file.
  * Updating years in copyright for 2020.
  * Updating to standards version 4.5.0.
  * Adding upstream version 0.13.

 -- Simon Spöhel <simon@spoehel.ch>  Fri, 24 Jan 2020 10:48:16 +0100

sentinelsat (0.12.2-2) unstable; urgency=medium

  * Updating year in copyright for 2019.
  * Updating to debhelper 12.
  * Updating to standards version 4.3.0.
  * Using PYBUILD variable in rules file.

 -- Simon Spöhel <simon@spoehel.ch>  Thu, 03 Jan 2019 14:14:52 +0100

sentinelsat (0.12.2-1) unstable; urgency=medium

  * Initial upload to sid (Closes: #907879).

 -- Simon Spöhel <simon@spoehel.ch>  Mon, 10 Sep 2018 12:05:44 +0200
